# Initial setup kubernetes konfig for GitLab

Project created as basis for setting up a GitLab cluster configuration for a newly created cluster e.g. at DigitalOcean https://www.digitalocean.com

Inspired by the discussion on  https://www.digitalocean.com/community/questions/adding-kubernetes-cluster-to-our-private-gitlab-server 

## just run setup.sh
setup.sh contains all steps. 

A ServiceAccount and a Namespace are created. 

At the end all important information needed for the configuration of Gitlab will be displayed.