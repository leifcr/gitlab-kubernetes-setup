#!/bin/bash
echo
echo ----  install dashboard  ----
echo
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
echo
echo ----  create namespace  ----
echo
kubectl create namespace gitlabkubesandbox
kubectl config set-context $(kubectl config current-context) --namespace=gitlabkubesandbox
echo
echo ----  create ServiceAccount for gitlab  ----
echo
kubectl create -f serviceAccount.yaml
kubectl create clusterrolebinding gitlab-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=gitlabkubesandbox:gitlab
echo
echo ----   inital setup done  ----
echo
echo
echo ServerURL = $(kubectl config view | grep server | cut -f 2- -d ":" | tr -d " ")
echo 
echo Certificate = $(kubectl get secret $(kubectl get secrets | grep gitlab-token | cut -f1 -d ' ') -o json | jq -r '.data["ca.crt"]' | base64 -D)
echo 
echo Token = $(kubectl describe secret $(kubectl get secrets | grep gitlab-token | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t')